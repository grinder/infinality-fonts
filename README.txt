Source do Infinality Fonts
https://github.com/bohoomil/fontconfig-ultimate
https://github.com/pdeljanov/infinality-remix

Essa minha versão do Infinality é baseada na versão do nihilismus
https://github.com/nihilismus/bob-infinality-bundle

Esse source foi construído principalmente para quem quiser usar a última versão
do Infinality Fonts

Requerimentos:
- slackware64 current

Conteúdo:
- freetype
- fontconfig
- cairo

Como instalar:
- você precisa baixar o .zip ou clonar o git
- então execute o script com o comando "sh build-infinality-fonts.sh"

------------------------------------------------------------------------------

Source of Infinality Fonts
https://github.com/bohoomil/fontconfig-ultimate
https://github.com/pdeljanov/infinality-remix

This my version of Infinality is based on the version of nihilismus
https://github.com/nihilismus/bob-infinality-bundle

This source was built primarily for those who want to use the latest version
the Infinality Fonts

Requirements:
- slackware64 current

Contents:
- freetype
- fontconfig
- cairo

How to install:
- you need to download the .zip or clone git
- then run the script "sh build-infinality-fonts.sh"
